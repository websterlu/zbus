package org.zbus.examples.ha.mq;

import org.zbus.broker.Broker;
import org.zbus.broker.ZbusBroker;
import org.zbus.mq.Producer;
import org.zbus.net.http.Message;

public class ProducerExample {
	public static void main(String[] args) throws Exception {
		Broker broker = new ZbusBroker("127.0.0.1:16666;127.0.0.1:16667");

		Producer producer = new Producer(broker, "MyMQ");
		producer.createMQ();

		for(int i=0;i<10000;i++){ 
			Message msg = new Message(); 
			msg.setBody("hello world " + System.currentTimeMillis());
			msg = producer.sendSync(msg);
			System.out.println(msg);
			Thread.sleep(2000);
		}

		broker.close();
	}
}
